# TP 3 IDM : Flux stochastiques – modèles du hasard - Nombres quasi-aléatoires - Parallélisme

## Prérequis
```bash
chmod u+x run.sh
```

## Note
Les mesures de temps d'exécution ont été effectués grâce à la classe [Chrono](src/chrono.hpp) utilisant le principe de [RAII](https://en.cppreference.com/w/cpp/language/raii), ce qui permet de mesurer précisément le temps passé dans un *scope*.

## Question 1
 Commande à exécuter:
 ```
 ./run.sh q1
 ```

## Question 2
Commande à exécuter:
```bash
./run.sh q2
```  
Résultat:
```cpp
Successful restore: true
```
On observe que deux générateurs Mersenne Twister initialisés avec des seeds différents auxquels on restore le même status fournissent le même résultat.

Afin de lancer une simulation de Monte Carlo en parallèle, nous pouvons dans un premier temps générer les fichier de status (voir fonction [generate_status](src/montecarlo.cpp#L23)). Il est alors possible de lancer 10 threads, chacun associé à un fichier de status différent. Il suffit alors de moyenner les résultats obtenus pour chaque thread afin d'obtenir le résultat qui aurait été obtenu de manière séquentielle.

## Question 4
Commande à exécuter:
```bash
./run.sh q4
```  
Résultat:
```cpp
generate_status 10 2'000'000'000: 78743 ms
3.14162
parallel_pi: 9325 ms
generate_status + parallel_pi: 88069 ms
3.14162
sequential_pi: 85899 ms
```

On remarque qu'une fois les status générés, le calcul de manière parallèle de `pi` s'effectue en `88,069ms`, contre `85,899ms` avec la méthode séquentielle.  
Cependant, dans la méthode parallèle, la majorité du temps est passée à générer les status. Cette opération n'ayant besoin d'être effectuée qu'une seule fois, il est alors possible de réutiliser ces status d'une exécution du programme à l'autre, permettant ainsi un calcul de `pi` effectué en seulement `9,325ms`.

## Question 5
Commande à exécuter:
```bash
./run.sh q5
```  
Résultat:
```cpp
Average draws number to get the word "gattaca": 14866.5
Theoretical probability: 6.10352e-05
Practical probability: 6.72652e-05
find_gattaca: 73 ms
```

On observe des probabilités théorique et réelle assez proches (variant de `10.2%`).  
Afin d'obtenir des résultats plus proches, il serait nécessaire d'augmenter le nombre de répétitions.

En ne considérant que les 26 lettres de l'alphabet, sans se soucier de la casse, des accents, des espaces ou de la ponctuation, la probabilité d'obtenir la chaîne "lehasardnecritpasdeprogramme" est de $`\frac{1}{28^{26}}`$, soit $`4.23^{-37}`$. Sachant que la génération de `20,000,000,000` de nombres aléatoires prend généralement plus de deux minutes, il est facile de voir en quoi la génération totalement aléatoire d'une telle phrase est impossible, car cella demanderait environs `28,000,000` d'années.

De même, la probabilité d'obtenir par hasard la chaîne fonctionnelle d'un humain est de $`\frac{1}{4^{3,000,000,000}}`$. En faisant l'approximation que $`2^{10} \approx 10^3`$, cela reviens donc à une probabilité d'environs $`10^{-1,800,000,000}`$.  
Il est alors évident que l'obtention "par hasard" d'une telle chaîne est totalement impossible avec les technologies actuelles.