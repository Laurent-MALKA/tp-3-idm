#include "word_generator.hpp"

WordGenerator::WordGenerator(CLHEP::MTwistEngine const& rng) : rng(rng) {}

std::string WordGenerator::generate_string(unsigned long long int string_size) {
    std::string result;
    result.reserve(string_size);

    for (unsigned long long int i = 0; i < string_size; ++i) {
        auto random_index = static_cast<unsigned int>(rng.flat() * dictionary.size());
        result.push_back(dictionary[random_index]);
    }

    return result;
}