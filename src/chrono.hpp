#if !defined(CHRONO_HEADER_INCLUDED)
#define CHRONO_HEADER_INCLUDED

#include <chrono>
#include <iostream>
#include <string>

class Chrono {
  public:
    Chrono(std::string const& message) : message(message), begin(std::chrono::steady_clock::now()) {}
    ~Chrono() {
        std::cout
            << message << ": "
            << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - begin).count()
            << " ms" << std::endl;
    }

  private:
    std::string message;
    std::chrono::steady_clock::time_point begin;
};

#endif // CHRONO_HEADER_INCLUDED
