#include "CLHEP/Random/MTwistEngine.h"
#include "chrono.hpp"
#include "constants.hpp"
#include "montecarlo.hpp"
#include "word_generator.hpp"
#include <cmath>
#include <fstream>
#include <iostream>

typedef unsigned long long int ull;

ull find_gattaca() {
    static WordGenerator wg{CLHEP::MTwistEngine{MT_SEED}};

    ull counter = 0;
    while (wg.generate_string(7) != "GATTACA") {
        ++counter;
    }

    return counter;
}

int main(int argc, char const* argv[]) {
    if (argc < 2) {
        std::cerr << "Usage: ./exe (2|4)\n";
        return EXIT_FAILURE;
    }

    int method;
    std::stringstream{argv[1]} >> method;

    if (method == 2) {
        Chrono chrono{"generate_status 10 10"};
        ull repetitions = 10;
        ull range = 10;
        generate_status(repetitions, range);
        // Generateurs initialisés avec des seeds différents
        CLHEP::MTwistEngine mt1{MT_SEED};
        CLHEP::MTwistEngine mt2{MT_SEED + 100};
        mt1.restoreStatus("2.conf");
        mt2.restoreStatus("2.conf");
        std::cout << "Successful restore: " << std::boolalpha
                  << (mt1.operator unsigned int() == mt2.operator unsigned int()) << "\n";
    } else if (method == 4) {
        ull repetitions = 10;
        ull range = 1'000'000'000;
        {
            Chrono chrono{"generate_status + parallel_pi"};
            {
                Chrono chrono{"generate_status 10 2'000'000'000"};
                generate_status(repetitions, range * 2);
            }
            {
                Chrono chrono{"parallel_pi"};
                std::cout << parallel_pi(repetitions, range) << std::endl;
            }
        }
        {
            Chrono chrono{"sequential_pi"};
            CLHEP::MTwistEngine rng{MT_SEED};
            std::cout << pi_estimation(rng, repetitions * range) << std::endl;
        }
    } else if (method == 5) {
        {
            Chrono chrono{"find_gattaca"};
            ull repetitions = 40;
            ull sum = 0;
            for (ull i = 0; i < repetitions; ++i) {
                sum += find_gattaca();
            }
            auto avg_draws = sum / static_cast<double>(repetitions);
            std::cout << "Average draws number to get the word \"gattaca\": " << avg_draws << std::endl;
            std::cout << "Theoretical probability: " << 1.0 / std::pow(4, 7) << std::endl;
            std::cout << "Practical probability: " << 1.0 / avg_draws << std::endl;
        }
    }

    return 0;
}
