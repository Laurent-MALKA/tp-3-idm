#if !defined(WORD_GENERATOR_INCLUDED)
#define WORD_GENERATOR_INCLUDED

#include "CLHEP/Random/MTwistEngine.h"
#include <string>
#include <vector>

class WordGenerator {
  public:
    WordGenerator(CLHEP::MTwistEngine const& rng);
    std::string generate_string(unsigned long long int string_size);

  private:
    std::vector<char> dictionary{'A', 'C', 'G', 'T'};
    CLHEP::MTwistEngine rng;
};

#endif // WORD_GENERATOR_INCLUDED
