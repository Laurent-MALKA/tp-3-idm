#if !defined(MONTECARLO_INCLUDED)
#define MONTECARLO_INCLUDED

#include "CLHEP/Random/MTwistEngine.h"

double pi_estimation(CLHEP::MTwistEngine& rng, unsigned long long int iterations);
void generate_status(unsigned long long int repetitions, unsigned long long int range);
double parallel_pi(unsigned long long int repetitions, unsigned long long int range);

#endif // MONTECARLO_INCLUDED
